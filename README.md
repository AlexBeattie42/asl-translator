# ASL Translator

## Cloning Repo
This repo has nested submodules so you need to use the `--recursive` flag 

You can choose ssh or https authentication.
I prefer ssh and will use that for this guide but you can always change urls accordingly.

Choose a directory to work in and clone the repo
```
git clone --recursive git@gitlab.com:alexbeattie42/asl-translator.git
```
## Installing Docker 

### Windows 10 Pro, Education 
1. https://docs.docker.com/docker-for-windows/install/
### Mac
1. https://docs.docker.com/docker-for-mac/install/
### Linux
1. Search how to install docker and docker-compose for your distro
### Windows 10 Home 
1. `Windows+R` then type  `msinfo32`
2. Look for Items that say virtualization and if they say yes proceed, otherwise proceed to Windows 10 Last Resort
3. Execute the `install_containers.bat` and `intall_hyper_v.bat` scripts as an admin 
(right-click, run as administrator) and wait untill they're done
4. Reboot when prompted
5. Download the Docker Desktop for Windows:
 https://hub.docker.com/editions/community/docker-ce-desktop-windows
6. Open registry editor `regedit.msc`
7. Navigate to the following folder: `\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion`
8. Change the following key: `EditionID` from `Core` => `Professional`
9. Run the docker installer downloaded in step 5
10. Follow prompts and restart when required. Confirm docker is running after restart
11. Change the key from step 8 back to `Core`
### Windows 10 Last Resort
* https://docs.docker.com/toolbox/toolbox_install_windows/

    or

* Install docker inside the WSL and follow the linux instructions

## Dev Environment Setup
#### (optional but it makes life easier)
1. Download and install PyCharm (Get the free student licence)
2. Open the root project folder for the repo you cloned `./asl-translator`
3. Setup Conda on your computer (preferred) or install python 3.7
4. 